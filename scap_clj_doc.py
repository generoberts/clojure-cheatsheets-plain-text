import requests
import bs4

url = 'https://jafingerhut.github.io/cheatsheet/clojuredocs/cheatsheet-use-title-attribute-no-cdocs-summary.html'

res = requests.get(url)
if res.status_code == requests.codes.ok:
    theSoup = bs4.BeautifulSoup(res.text, features='lxml')
    all_nodes = theSoup.select('a')
    with open('clj_doc.txt', 'w') as file:
        for node in all_nodes:
            # print(node.get('title'))
            # print(node.get_text())
            if node.get('title') != None:
                file.write(node.get_text())
                file.write('\n')
                file.write(node.get('title'))
                file.write('\n\n')
