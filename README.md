# Introduction

A Github repository whose purpose is to be published here:
[http://jafingerhut.github.io](http://jafingerhut.github.io)

Most of it is related to the programming language
[Clojure](https://clojure.org).

There is also some information
[here](property-based-testing/README.md) about a style of software
testing known by several names, including property-based testing and
specification-based testing.

######################################################################

Cheatsheets from https://jafingerhut.github.io/ Extract and convert its content to a text file for easier reading.
Usage: just run `python scap_clj_doc.py`