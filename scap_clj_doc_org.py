import requests
import bs4

url = 'https://jafingerhut.github.io/cheatsheet/clojuredocs/cheatsheet-use-title-attribute-no-cdocs-summary.html'

res = requests.get(url)
if res.status_code == requests.codes.ok:
    theSoup = bs4.BeautifulSoup(res.text, features='lxml')
    all_nodes = theSoup.select('a')
    with open('clj_doc2.org', 'w') as file:
        for node in all_nodes:
            # print(node.get('title'))
            # print(node.get_text())
            if node.get('title') != None:
                file.write('* ' + node.get_text())
                file.write('\n')
                contents = node.get('title').split('\n')
                if len(contents) < 2:
                    file.write('\n'.join(contents))
                else:
                    file.write('#+BEGIN_SRC clojure\n')
                    file.write(contents[0])
                    file.write('\n')
                    file.write(contents[1])
                    file.write('\n#+END_SRC\n')
                    # file.write(contents[2:].join('\n'))
                    file.write('\n'.join(contents[2:]))
                    file.write('\n\n')
